use chrono::prelude::*;
use deserialize_xml::{impl_deserialize_xml_helper, DeserializeXml};

#[derive(Default, Debug, DeserializeXml)]
struct StringOnly {
    title: String,
    author: String,
}

#[derive(Default, Debug, DeserializeXml)]
struct Nested {
    root: String,
    child: StringOnly,
}

#[derive(Default, Debug, DeserializeXml)]
struct NestedVec {
    root: String,
    children: Vec<StringOnly>,
}

#[derive(Default, Debug, DeserializeXml)]
struct Channel {
    title: String,
    description: String,
    generator: String,
    entries: Vec<Item>,
}

#[derive(Default, Debug, DeserializeXml)]
struct Item {
    title: String,
    link: String,
    author: String,
    #[deserialize_xml(tag = "pubDate")]
    pub_date: String,
    description: String,
    guid: String,
}

#[derive(Default, Debug, DeserializeXml)]
#[deserialize_xml(tag = "renamed")]
struct TagAttrTest {
    title: String,
    #[deserialize_xml(tag = "writer")]
    author: String,
}

#[derive(Default, Debug, DeserializeXml)]
struct MixedTypes {
    title: String,
    ttl: u32,
    eliteness: i16,
}

#[derive(Default, Debug, DeserializeXml)]
#[deserialize_xml(tag = "outer")]
struct CustomImplHelperOuter {
    #[deserialize_xml(tag = "inner")]
    dt: CustomImplHelperInner,
}

#[derive(Default, Debug)]
struct CustomImplHelperInner(DateTime<Utc>);

impl_deserialize_xml_helper!(CustomImplHelperInner, tag_contents, {
    let dt = tag_contents.parse::<DateTime<Utc>>()?;
    Ok(CustomImplHelperInner(dt))
});

#[derive(Default, Debug, DeserializeXml)]
struct Comment {
    title: String,
    author: String,
}

#[derive(Default, Debug, DeserializeXml)]
struct TestOption {
    title: String,
    subtitle: Option<String>,
    comment: Option<Comment>,
}


#[derive(Default, Debug, DeserializeXml)]
#[deserialize_xml(tag = "comment")]
struct AnonymousComment {
    id: u8,
    author: Option<String>,
    message: String,
    // This attribute is annoying... maybe revisit the assumption that Vec<T> fields should use T
    // as the tag name by default?
    #[deserialize_xml(tag = "reply_to")]
    reply_to: Vec<u8>,
}

#[derive(Default, Debug, DeserializeXml)]
struct KitchenSink {
    title: String,
    subtitle: Option<String>,
    #[deserialize_xml(tag = "comment")]
    comments: Vec<AnonymousComment>,
    categories: Vec<String>,
}

fn main() {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{env, fs, path::PathBuf};

    #[test]
    fn test_str_only() {
        let str_input =
            "<xml><stringonly><title>Title</title><author>Author</author></stringonly></xml>";
        let result = StringOnly::from_str(str_input).unwrap();
        assert_eq!(result.title, "Title");
        assert_eq!(result.author, "Author");
    }

    #[test]
    fn test_nested_basic() {
        let input = "<xml><nested><root>Root</root><child><stringonly><title>Nested title</title><author>Nested author</author></stringonly></child></nested></xml>";

        let result = Nested::from_str(input).unwrap();
        assert_eq!(result.root, "Root");
        assert_eq!(result.child.title, "Nested title");
        assert_eq!(result.child.author, "Nested author");
    }

    #[test]
    fn test_nested_vec() {
        let input = r#"<xml>
            <nestedvec>
              <root>Root</root>
              <stringonly><title>Article 1</title><author>Guy</author></stringonly>
              <stringonly><title>Article 2</title><author>Dude</author></stringonly>
            </nestedvec></xml>"#;

        let result = NestedVec::from_str(input).unwrap();
        assert_eq!(result.root, "Root");
        assert_eq!(result.children.len(), 2);
        assert_eq!(result.children[0].title, "Article 1");
        assert_eq!(result.children[0].author, "Guy");
        assert_eq!(result.children[1].title, "Article 2");
        assert_eq!(result.children[1].author, "Dude");
    }

    #[test]
    fn test_attr_tag() {
        let input = r#"<xml>
            <renamed>
              <title>Macros Galore!</title>
              <writer>A Rustacean</writer>
            </renamed></xml>"#;

        // Without using our "tag" attribute, this should look for <stringonly> tags only--since
        // there aren't any, all fields should be empty
        let default_result = StringOnly::from_str(input).unwrap();
        assert_eq!(default_result.title, "");
        assert_eq!(default_result.author, "");

        // ...but after renaming, things should work as expected (see the "tag" attributes on
        // TagAttrTest)
        let renamed_result = TagAttrTest::from_str(input).unwrap();
        assert_eq!(renamed_result.title, "Macros Galore!");
        assert_eq!(renamed_result.author, "A Rustacean");
    }

    #[test]
    fn test_mixed_types() {
        let input = r#"<xml>
            <mixedtypes>
              <title>This ain't gonna last</title>
              <ttl>60</ttl>
              <eliteness>-1337</eliteness>
            </mixedtypes></xml>"#;

        let result = MixedTypes::from_str(input).unwrap();
        assert_eq!(result.title, "This ain't gonna last");
        assert_eq!(result.ttl, 60);
        assert_eq!(result.eliteness, -1337);
    }

    #[test]
    fn test_real_rss() {
        let rss_file: PathBuf = [
            &env::var("CARGO_MANIFEST_DIR").unwrap(),
            "test_data",
            "gowers.xml",
        ]
        .iter()
        .collect();
        let rss_raw_data = fs::read_to_string(rss_file).unwrap();

        let result = Channel::from_str(&rss_raw_data).unwrap();
        assert_eq!(result.title, "Gowers's Weblog");
        assert_eq!(result.description, "Mathematics related discussions");
        assert_eq!(result.generator, "http://wordpress.com/");
        assert_eq!(result.entries.len(), 25);
        assert_eq!(
            result.entries[0].title,
            "Announcing an automatic theorem proving project"
        );
        assert_eq!(result.entries[0].link, "https://gowers.wordpress.com/2022/04/28/announcing-an-automatic-theorem-proving-project/");
        // This feed uses <dc:creator> tags--seems weird, but let's roll with it
        assert_eq!(result.entries[0].author, "");
        assert_eq!(
            result.entries[0].pub_date,
            "Thu, 28 Apr 2022 09:41:22 +0000"
        );
        assert_eq!(
            result.entries[0].guid,
            "http://gowers.wordpress.com/?p=6531"
        );
        assert_eq!(
            result.entries[1].title,
            "Leicester mathematics under threat again"
        );
        assert_eq!(
            result.entries[1].link,
            "https://gowers.wordpress.com/2021/01/30/leicester-mathematics-under-threat-again/"
        );
        assert_eq!(
            result.entries[1].guid,
            "http://gowers.wordpress.com/?p=6526"
        );
        assert_eq!(result.entries[24].title, "Time for Elsexit?");
        assert_eq!(
            result.entries[24].link,
            "https://gowers.wordpress.com/2016/11/29/time-for-elsexit/"
        );
        assert_eq!(
            result.entries[24].guid,
            "http://gowers.wordpress.com/?p=6207"
        );

        // Test that we didn't miss any of these required elements
        for entry in &result.entries {
            assert_ne!(entry.title, "");
            assert_ne!(entry.link, "");
            assert_ne!(entry.description, "");
            assert_ne!(entry.guid, "");
            assert_ne!(entry.pub_date, "");
        }
    }

    #[test]
    fn test_impl_helper_macro() {
        let str_input = "<outer><inner>1918-11-11T11:00:00+01:00</inner></outer>";
        let result = CustomImplHelperOuter::from_str(str_input).unwrap();
        assert_eq!(result.dt.0.year(), 1918);
        assert_eq!(result.dt.0.month(), 11);
        assert_eq!(result.dt.0.day(), 11);
        assert_eq!(result.dt.0.hour(), 10);
    }

    #[test]
    fn test_option() {
        let str_input = r#"<testoption>
            <title>Moon pie</title>
            <comment>
              <author>Jasper Beardsley</author>
              <title>What a time to be alive.</title>
            </comment>
            </testoption>"#;
        let result = TestOption::from_str(str_input).unwrap();
        assert_eq!(result.title, "Moon pie");
        assert_eq!(result.subtitle, None);
        let comment = result.comment.unwrap();
        assert_eq!(comment.author, "Jasper Beardsley");
        assert_eq!(comment.title, "What a time to be alive.");
    }

    #[test]
    fn test_kitchen_sink() {
        let input = r#"<kitchensink>
            <title>Welcome thread</title>
            <subtitle>Introduce yourselves!</subtitle>
            <comment>
                <id>0</id>
                <message>hullo i am anonymoose</message>
            </comment>
            <comment>
                <id>1</id>
                <reply_to>0</reply_to>
                <author>mr_friendly</author>
                <message>Hallo, anonymous!</message>
            </comment>
            <comment>
                <id>2</id>
                <reply_to>0</reply_to>
                <reply_to>1</reply_to>
                <message>welcome</message>
            </comment>
            </kitchensink>
            "#;

        let result = KitchenSink::from_str(input).unwrap();
        assert_eq!(result.title, "Welcome thread");
        assert_eq!(result.subtitle.unwrap(), "Introduce yourselves!");
        let comments = result.comments;
        assert_eq!(comments.len(), 3);
        assert_eq!(comments[0].id, 0);
        assert_eq!(comments[0].author, None);
        assert_eq!(comments[0].message, "hullo i am anonymoose");
        assert_eq!(comments[0].reply_to.len(), 0);
        assert_eq!(comments[1].id, 1);
        assert_eq!(comments[1].author.as_ref().unwrap(), "mr_friendly");
        assert_eq!(comments[1].message, "Hallo, anonymous!");
        assert_eq!(comments[1].reply_to.len(), 1);
        assert_eq!(comments[1].reply_to[0], 0);
        assert_eq!(comments[2].id, 2);
        assert_eq!(comments[2].author, None);
        assert_eq!(comments[2].message, "welcome");
        assert_eq!(comments[2].reply_to.len(), 2);
        assert_eq!(comments[2].reply_to[0], 0);
        assert_eq!(comments[2].reply_to[1], 1);
        assert_eq!(result.categories.len(), 0);
    }
}
